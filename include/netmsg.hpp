#ifndef LIGHTDS_NETMSG_HPP
#define LIGHTDS_NETMSG_HPP

#include <string>
#include <vector>
#include <functional>

namespace LightDS
{
	class NetMessage
	{
	public:
		enum CompressFlag : std::uint8_t
		{
			None = 0,
		};

	public:
		NetMessage() : length(0) {}

		void onRecv(const std::vector<char> &data, size_t nLen)
		{
			for (size_t i = 0; i < nLen && i < data.size(); i++)
				onRecv(data[i]);
		}

		void onRecv(char c)
		{
			buffer.push_back(c);
			if (buffer.size() == 4)
			{
				length = (
					(std::uint8_t(buffer[0]) << 24) |
					(std::uint8_t(buffer[1]) << 16) |
					(std::uint8_t(buffer[2]) << 8) |
					std::uint8_t(buffer[3]));
			}
			else if (buffer.size() == 5)
				compressFlag = CompressFlag(buffer[4]);
			else if (buffer.size() == length + 5)
			{
				//TODO: Uncompress Data
				msgHandler(buffer.substr(5));
				buffer.clear();
				length = 0;
				compressFlag = None;
			}
		}

		void setMsgHandler(std::function<void(std::string)> func)
		{
			msgHandler = func;
		}

		static std::string sendMsg(const std::string &msg)
		{
			//TODO: Throw exception if len(msg) > UINT32_MAX
			std::uint32_t len = msg.size();
			std::string temp;
			temp.push_back(char((len >> 24) & 0xff));
			temp.push_back(char((len >> 16) & 0xff));
			temp.push_back(char((len >> 8) & 0xff));
			temp.push_back(char(len & 0xff));
			temp.push_back(char(CompressFlag::None));
			temp.append(msg);
			return temp;
		}

	protected:
		std::string		buffer;
		std::uint32_t	length;
		CompressFlag	compressFlag;

		std::function<void(std::string)> msgHandler;
	};
}

#endif
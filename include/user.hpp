#ifndef LIGHTDS_USER_HPP
#define LIGHTDS_USER_HPP

#include "netclient.hpp"
#include "rpc.hpp"
#include "pipeservice.hpp"

namespace LightDS
{
	class User
	{
	public:
		struct RPCAddress
		{
			std::string		ip;
			std::uint16_t	port;

			std::string to_string() const
			{
				return ip + ":" + std::to_string(port);
			}
			static RPCAddress from_string(const std::string &str)
			{
				size_t colon = str.find_last_of(':');
				if (colon == std::string::npos || colon == str.length() - 1)
					throw std::invalid_argument("Invalid RPCAddress");
				RPCAddress addr;
				addr.ip = str.substr(0, colon);
				addr.port = std::stoi(str.substr(colon + 1));
				return addr;
			}
		};

	public:
		explicit User(RPCAddress addrNodeServer, const std::string &localAddr = "") : 
			netClient(32, localAddr),
			rpcClientToNodeServer(getRPCClient(addrNodeServer))
		{
		}
		explicit User(PipeService &pipeSrv, const std::string &localAddr = "") :
			netClient(32, localAddr)
		{
			rpcClientToNodeServer.setSyncReq([&pipeSrv](const std::string &msg) -> std::string
			{
				return pipeSrv.sendRequest(msg);
			});
			rpcClientToNodeServer.setAsyncSend([&pipeSrv](const std::string &msg, std::function<void(std::string, std::string)> callback)
			{
				return pipeSrv.sendAsyncRequest(msg, callback);
			});
		}

		User(const User &) = delete;

		void Run()
		{
			netClient.Run();
		}

	public:
		template<typename ...Args>
		auto RPCCall(RPCAddress addr, const std::string &rpcName, const Args &...args)
		{
			RPCClient rpcClient = getRPCClient(addr);
			return rpcClient.call(rpcName, args...);
		}

		template<typename ...Args>
		auto RPCAsyncCall(RPCAddress addr, const std::string &rpcName, const Args &...args)
		{
			RPCClient rpcClient = getRPCClient(addr);
			return rpcClient.call_async(rpcName, args...);
		}

		std::vector<RPCAddress> ListService(const std::string &serviceName)
		{
			std::vector<std::string> addrs = rpcClientToNodeServer.call("ListService", serviceName).get().as<std::vector<std::string>>();
			std::vector<RPCAddress> ret;
			for (auto &addr : addrs)
				ret.push_back(RPCAddress::from_string(addr));
			return ret;
		}

	protected:
		RPCClient getRPCClient(RPCAddress addr)
		{
			RPCClient rpcClient;
			rpcClient.setSyncReq([this, addr](const std::string &req) -> std::string
			{
				return netClient.sendRequest(req, addr.ip, addr.port);
			});
			rpcClient.setAsyncSend([this, addr](const std::string &req, std::function<void(std::string, std::string)> callback)
			{
				netClient.sendAsyncRequest(req, addr.ip, addr.port, callback);
			});
			return rpcClient;
		}

	protected:
		NetClient		 netClient;
		RPCClient		 rpcClientToNodeServer;
	};
}

#endif
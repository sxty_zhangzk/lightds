#ifndef LIGHTDS_PIPE_SERVICE_HPP
#define LIGHTDS_PIPE_SERVICE_HPP

#include <iostream>
#include <string>
#include <map>
#include <functional>
#include <mutex>
#include <atomic>
#include <json/json.h>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>

namespace LightDS
{
	class PipeService
	{
	public:
		PipeService(std::istream &in, std::ostream &out) :
			in(in), out(out), cntRequest(0), stopped(true)
		{

		}

		void Run()
		{
			stopped = false;
			std::string line;
			while (std::getline(in, line))
			{
				if (!onRecv(line))
					break;
			}
		}

		void Stop()
		{
			{
				std::lock_guard<std::mutex> lock(mtxRequest);
				if (stopped.exchange(true))
					return;

				for (auto &kv : mapRequestHandler)
					kv.second("");
				mapRequestHandler.clear();
			}

			Json::Value root;
			root["type"] = "bye";

			Json::FastWriter writer;
			writer.omitEndingLineFeed();
			std::lock_guard<std::mutex> lockOut(mtxOut);
			out << writer.write(root) << std::endl;
			out.flush();
		}

		std::string sendRequest(const std::string &msg)
		{
			std::uint64_t idxRequest = postRequest(msg);

			std::condition_variable cvDone;
			std::mutex mtxDone;
			std::string strResponse;

			{
				std::lock_guard<std::mutex> lock(mtxRequest);
				if (stopped)
					return "";
				mapRequestHandler[idxRequest] = [&cvDone, &mtxDone, &strResponse](std::string response)
				{
					std::lock_guard<std::mutex> lockDone(mtxDone);
					strResponse = std::move(response);
					cvDone.notify_one();
				};
			}

			std::unique_lock<std::mutex> lockDone(mtxDone);
			cvDone.wait(lockDone);

			return base64Decode(strResponse);
		}

		// NOTE: sendRequest should not be called in `callback`
		void sendAsyncRequest(const std::string &msg,
			std::function<void(std::string /*buf*/, std::string /*error*/)> callback)
		{
			std::uint64_t idxRequest = postRequest(msg);

			{
				std::lock_guard<std::mutex> lock(mtxRequest);
				if (stopped)
				{
					callback("", "");
					return;
				}
				mapRequestHandler[idxRequest] = [callback](std::string response)
				{
					std::string responseDecoded, errorDescription;
					try
					{
						responseDecoded = base64Decode(response);
					}
					catch (std::exception &e)
					{
						errorDescription = e.what();
					}
					callback(std::move(responseDecoded), std::move(errorDescription));
				};
			}
		}

		void setReqHandler(std::function<std::string(std::string)> func)
		{
			reqHandler = func;
		}

	protected:
		std::uint64_t postRequest(const std::string &msg)
		{
			std::uint64_t idxRequest = cntRequest++;
			Json::Value root;
			root["type"] = "request";
			root["reqid"] = idxRequest;
			root["data"] = base64Encode(msg);

			Json::FastWriter writer;
			writer.omitEndingLineFeed();

			{
				std::lock_guard<std::mutex> lockOut(mtxOut);
				out << writer.write(root) << std::endl;
				out.flush();
			}

			return idxRequest;
		}

		bool onRecv(const std::string &line)	//return false to end the session
		{
			Json::Reader reader;
			Json::Value root;
			reader.parse(line, root);
			std::string type = root["type"].asString();
			if (type == "bye")
			{
				Stop();
				return false;
			}
			if (type == "request")
			{
				std::uint64_t idxRequest = root["reqid"].asUInt64();
				std::string strRequest = base64Decode(root["data"].asString());
				std::thread threadHandler([this, idxRequest, strRequest = std::move(strRequest)]() mutable
				{
					std::string response = reqHandler(std::move(strRequest));
					Json::Value responseRoot;
					responseRoot["type"] = "response";
					responseRoot["reqid"] = idxRequest;
					responseRoot["data"] = base64Encode(response);

					Json::FastWriter writer;
					writer.omitEndingLineFeed();

					{
						std::lock_guard<std::mutex> lockOut(mtxOut);
						out << writer.write(responseRoot) << std::endl;
						out.flush();
					}
				});
				threadHandler.detach();
			}
			else if (type == "response")
			{
				std::uint64_t idxResponse = root["reqid"].asUInt64();
				std::string strResponse = root["data"].asString();

				std::lock_guard<std::mutex> lock(mtxRequest);
				if (mapRequestHandler.count(idxResponse))
				{
					mapRequestHandler[idxResponse](std::move(strResponse));
					mapRequestHandler.erase(idxResponse);
				}
			}

			return true;
		}

		static std::string base64Encode(const std::string &data)
		{
			using namespace boost::archive::iterators;
			typedef base64_from_binary<transform_width<std::string::const_iterator, 6, 8>> base64enc;
			std::string ret;
			std::copy(base64enc(data.begin()), base64enc(data.end()), std::back_inserter(ret));
			//ret.append((3 - ret.size() % 3) % 3, '=');
			return ret;
		}
		static std::string base64Decode(const std::string &data)
		{
			using namespace boost::archive::iterators;
			typedef transform_width<binary_from_base64<std::string::const_iterator>, 8, 6> base64dec;
			std::string ret;
			std::copy(base64dec(data.begin()), base64dec(data.end()), std::back_inserter(ret));
			return ret;
		}

	protected:
		std::istream			&in;
		std::ostream			&out;
		std::mutex				 mtxOut;

		std::atomic<std::uint64_t> cntRequest;

		std::mutex				 mtxRequest;
		std::map<std::uint64_t, std::function<void(std::string)>> mapRequestHandler;

		std::function<std::string(std::string)> reqHandler;

		std::atomic<bool>		stopped;
	};
}

#endif
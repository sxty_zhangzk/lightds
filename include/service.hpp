#ifndef LIGHTDS_SERVICE_HPP
#define LIGHTDS_SERVICE_HPP

#include <iostream>
#include <string>
#include <map>
#include <mutex>
#include <functional>
#include <boost/utility/base_from_member.hpp>
#include "netserver.hpp"
#include "rpc.hpp"
#include "log.hpp"
#include "user.hpp"

namespace LightDS
{
	class Service : 
		protected boost::base_from_member<PipeService>,
		public User
	{
	protected:
		struct ThreadContext
		{
			std::string		callerIP;
			std::uint16_t	callerPort;
		};
		using pipeService = boost::base_from_member<PipeService>;

	public:
		Service(const std::string &srvName, std::ostream &logfile, std::istream &in = std::cin, std::ostream &out = std::cout, std::uint16_t rpcPort = 0, const std::string &localAddr = "") :
			pipeService(in, out),
			User(pipeService::member, localAddr),
			srvName(srvName), rpcPort(rpcPort),
			netServer(
				localAddr.empty()
				? boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), rpcPort)
				: boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(localAddr), rpcPort)
			),
			logStderr(std::cerr), logFile(logfile, false),
			logger({&logStderr, &logFile}),
			localAddr(localAddr),
			nBusyThread(0)
		{
			netServer.setReqHandler([this](const std::string &req, const std::string &ip, std::uint16_t port) -> std::string
			{
				{
					std::lock_guard<std::mutex> lock(mtxThreadContext);
					timeLastEnter = std::chrono::high_resolution_clock::now();
					mapThreadContext[std::this_thread::get_id()] = ThreadContext{ ip, port };
					nBusyThread++;
				}
				std::shared_ptr<void> defer(nullptr, [this](const void *)
				{
					std::lock_guard<std::mutex> lock(mtxThreadContext);
					nBusyThread--;
				});
				return rpcServer.onRequest(req);
			});
		}
		Service(const Service &) = delete;
		~Service()
		{
			Stop();
		}

		void Run()
		{
			User::Run();

			std::mutex mtxStop;
			std::condition_variable cvStop;
			bool stopped = false;
			
			unsigned int nServerThread = std::thread::hardware_concurrency() * 2;
			nBusyThread = 0;
			netServer.Run(nServerThread);

			// auto increase the number of worker thread to avoid possible deadlock in RPC
			std::thread workerIncreaser([this, &nServerThread, &mtxStop, &cvStop, &stopped]()
			{
				using namespace std::chrono_literals;
				std::chrono::milliseconds waitTime = 100ms;
				std::unique_lock<std::mutex> lockStop(mtxStop);
				for (; !stopped; cvStop.wait_for(lockStop, waitTime))
				{
					std::lock_guard<std::mutex> lock(mtxThreadContext);
					if (nBusyThread >= nServerThread &&
						std::chrono::high_resolution_clock::now() - timeLastEnter > waitTime * 2)
					{
						//TODO: Don't start new thread when cpu usage is high (computationally intensive task)
						nServerThread++;
						netServer.Run(1);
						waitTime += 100ms;
					}
				}
			});

			if (funcOnStart)
				funcOnStart();

			User::rpcClientToNodeServer.call_async("RegisterRPCService", srvName, localAddr, netServer.getLocalEndpoint().port());
			pipeService::member.Run();

			if (funcOnStop)
				funcOnStop();

			{
				std::lock_guard<std::mutex> lockStop(mtxStop);
				stopped = true;
				cvStop.notify_all();
			}

			workerIncreaser.join();
		}

		void Stop()
		{
			//TODO
			pipeService::member.Stop();
		}

		void setOnStart(std::function<void()> func)
		{
			funcOnStart = func;
		}
		void setOnStop(std::function<void()> func)
		{
			funcOnStop = func;
		}

	public:
		template<typename T, typename Callable>
		void RPCBind(const std::string &rpcName, Callable func)
		{
			rpcServer.bind(rpcName, std::function<T>(func));
		}
		template<typename T>
		void RPCBind(const std::string &rpcName, std::function<T> func)
		{
			rpcServer.bind(rpcName, func);
		}

		std::string getRPCCaller() const
		{
			std::lock_guard<std::mutex> lock(mtxThreadContext);
			if (!mapThreadContext.count(std::this_thread::get_id()))
				return "";
			return mapThreadContext.find(std::this_thread::get_id())->second.callerIP;
		}

		std::uint16_t getLocalRPCPort() const
		{
			return netServer.getLocalEndpoint().port();
		}

		template<typename ...T>
		void log(LogLevel level, T &&...args)
		{
			logger.log(level, std::forward<T>(args)...);
		}
		void logf(LogLevel level, const char *fmt, ...)
		{
			va_list args;
			va_start(args, fmt);
			logger.vlogf(level, fmt, args);
			va_end(args);
		}

	protected:
		bool			 running;
		std::string		 srvName;
		std::uint16_t	 rpcPort;
		/*std::istream	&in;
		std::ostream	&out;*/
		StreamWriter	 logStderr, logFile;
		Log				 logger;
		NetServer		 netServer;
		RPCServer		 rpcServer;

		mutable std::mutex	 mtxThreadContext;
		std::map<std::thread::id, ThreadContext> mapThreadContext;
		unsigned int nBusyThread;
		std::chrono::high_resolution_clock::time_point timeLastEnter;
		
		std::function<void()> funcOnStart;
		std::function<void()> funcOnStop;

		std::string		 localAddr;
	};
}

#endif

#ifndef LIGHTDS_RPC_HPP
#define LIGHTDS_RPC_HPP

#include <tuple>
#include <unordered_map>
#include <functional>
#include <future>
#include <stdexcept>
#include <sstream>
#include <msgpack.hpp>


namespace LightDS
{
	//Request Format:
	//	RPC <rpcname>\n<args in msgpack>
	//Response Format:
	//	OK\n<ret in msgpack>
	//	FAIL\n<error code>\n<error description>

	enum class RPCErrorCode : int
	{
		BadResponse = 0,
		BadRequest = 1,
		ProcedureNotFound = 2,
		TransmitError = 3,
		InvalidArgument = 4,
	};

	class RPCException : public std::exception
	{
	public:
		RPCException(RPCErrorCode code, std::string description) :
			errCode(code), description(description) {}

		virtual const char * what() const noexcept override
		{
			return description.c_str();
		}

		RPCErrorCode code() const
		{
			return errCode;
		}
	private:
		const RPCErrorCode errCode;
		const std::string description;
	};

	class RPCServer
	{
	public:
		std::string onRequest(const std::string &request)
		{
			if (request.substr(0, 4) != "RPC ")
			{
				//Bad Request
				return "FAIL\n" + std::to_string(int(RPCErrorCode::BadRequest)) + "\n" + "Bad Request";
			}
			std::string rpcName;
			for (size_t i = 4; ; i++)
			{
				if (i >= request.size())
				{
					//Bad Request
					return "FAIL\n" + std::to_string(int(RPCErrorCode::BadRequest)) + "\n" + "Bad Request";
				}
				if (request[i] == '\n')
					break;
				rpcName.push_back(request[i]);
			}

			if (!funcList.count(rpcName))
			{
				//Procedure Not Found
				return "FAIL\n" + std::to_string(int(RPCErrorCode::ProcedureNotFound)) + "\n" + "Procedure Not Found";
			}

			return funcList[rpcName](request.substr(4 + rpcName.size() + 1));
		}

		template<typename Ret, typename ...Args>
		void bind(const std::string &rpcName, std::function<Ret(Args...)> func)
		{
			funcList[rpcName] = [func](const std::string &request) { return dispatch(func, request); };
		}

	protected:
		template<typename Ret>
		static Ret dispatch_by_args(std::function<Ret()> func, const std::string &request)
		{
			if (!request.empty())
				throw std::invalid_argument("Unexpected Argument");
			return func();
		}
		template<typename Ret, typename ...Args>
		static Ret dispatch_by_args(std::function<Ret(Args...)> func, const std::string &request)
		{
			auto handle = msgpack::unpack(request.data(), request.size());
			std::tuple<std::decay_t<Args>...> args = handle.get().as<std::tuple<std::decay_t<Args>...>>();
			return apply(func, std::move(args));
		}
		template<typename ...Args>
		static std::string dispatch_by_ret(std::function<void(Args...)> func, const std::string &request)
		{
			dispatch_by_args(func, request);
			return "OK\n";
		}
		template<typename Ret, typename ...Args>
		static std::string dispatch_by_ret(std::function<Ret(Args...)> func, const std::string &request)
		{
			auto ret = dispatch_by_args(func, request);
			std::stringstream ss;
			msgpack::pack(ss, ret);
			return "OK\n" + ss.str();
		}
		template<typename Ret, typename ...Args>
		static std::string dispatch(std::function<Ret(Args...)> func, const std::string &request)
		{
			try
			{
				return dispatch_by_ret(func, request);
			}
			catch (std::invalid_argument &e)
			{
				return "FAIL\n" + std::to_string(int(RPCErrorCode::InvalidArgument)) + "\n" + e.what();
			}
			catch (msgpack::unpack_error &e)
			{
				return "FAIL\n" + std::to_string(int(RPCErrorCode::BadRequest)) + "\n" + e.what();
			}
			catch (msgpack::type_error &e)
			{
				return "FAIL\n" + std::to_string(int(RPCErrorCode::InvalidArgument)) + "\n" + e.what();
			}
		}

	private:
		template<typename Tuple, typename Ret, typename ...Args, std::size_t ...I>
		static decltype(auto) apply_impl(std::function<Ret(Args...)> func, Tuple &&t, std::index_sequence<I...>)
		{
			return func(std::get<I>(std::forward<Tuple>(t))...);
		}

		template<typename Tuple, typename Ret, typename ...Args>
		static decltype(auto) apply(std::function<Ret(Args...)> func, Tuple &&t)
		{
			return apply_impl(func, std::forward<Tuple>(t), std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
		}

	protected:
		std::unordered_map<std::string, std::function<std::string(const std::string &)>> funcList;
	};

	class RPCClient
	{
	public:
		typedef std::function<
			void(
				const std::string &,
				std::function <void(std::string /*buf*/, std::string /*error*/)>
				)> funcAsyncSend_t;
		typedef std::function<std::string(const std::string &)> funcSyncSend_t;
	public:
		void setAsyncSend(funcAsyncSend_t func) { funcAsyncSend = func; }
		void setSyncReq(funcSyncSend_t func) { funcSyncSend = func; }

		template<typename ...Args>
		std::future<msgpack::object_handle> call_async(const std::string &rpcName, const Args &...args)
		{
			// std::promise is not copy-constructible while std::function requires a copy constructible function
			std::shared_ptr<std::promise<msgpack::object_handle>> promise = std::make_shared<std::promise<msgpack::object_handle>>();
			std::future<msgpack::object_handle> future = promise->get_future();
			funcAsyncSend(generateRequest(rpcName, args...),
				[promise](std::string buf, std::string error)  {
				OnAsyncRecv(*promise, buf, error);
			});
			return future;
		}

		template<typename ...Args>
		msgpack::object_handle call(const std::string &rpcName, const Args &...args)
		{
			std::string response;
			try
			{
				response = funcSyncSend(generateRequest(rpcName, args...));
			}
			catch (std::exception &e)
			{
				throw RPCException(RPCErrorCode::TransmitError, e.what());
			}
			return parseResponse(response);
		}

	protected:
		static std::string generateRequest(const std::string &rpcName)
		{
			return "RPC " + rpcName + "\n";
		}
		template<typename ...Args>
		static std::string generateRequest(const std::string &rpcName, const Args &...args)
		{
			std::tuple<const Args &...> tupleArgs = std::tie(args...);
			std::stringstream ss;
			msgpack::pack(ss, tupleArgs);
			return "RPC " + rpcName + "\n" + ss.str();
		}

		static void OnAsyncRecv(std::promise<msgpack::object_handle> &promise, const std::string &buf, const std::string &error)
		{
			if (!error.empty())
			{
				promise.set_exception(std::make_exception_ptr(RPCException(RPCErrorCode::TransmitError, error)));
				return;
			}
			try
			{
				promise.set_value(parseResponse(buf));
			}
			catch (RPCException &e)
			{
				promise.set_exception(std::make_exception_ptr(e));
			}
		}

		static msgpack::object_handle parseResponse(const std::string &response)
		{
			std::string status = response.substr(0, response.find_first_of('\n'));
			std::string remain = status.size() + 1 < response.size() ? response.substr(status.size()+1) : "";
			if (status == "OK")
			{
				if (remain.empty())
					return msgpack::object_handle();

				return msgpack::unpack(remain.data(), remain.size());
			}
			else if (status == "FAIL")
			{
				std::string strErrCode = remain.substr(0, remain.find_first_of('\n'));
				int code;
				try
				{
					code = std::stoi(strErrCode);
				}
				catch (std::exception &)
				{
					throw RPCException(RPCErrorCode::BadResponse, "Bad Response");
				}
				std::string description = strErrCode.size() + 1 < remain.size() ? remain.substr(strErrCode.size() + 1) : "";
				throw RPCException(RPCErrorCode(code), description);
			}
			else
			{
				throw RPCException(RPCErrorCode::BadResponse, "Bad Response");
			}
		}

	protected:
		funcAsyncSend_t funcAsyncSend;
		funcSyncSend_t  funcSyncSend;
	};
}

#endif

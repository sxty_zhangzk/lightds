#ifndef LIGHTDS_POOL_HPP
#define LIGHTDS_POOL_HPP

#include <functional>
#include <memory>
#include <map>
#include <set>
#include <list>
#include <mutex>

namespace LightDS
{
	template<typename Key, typename Value>
	class KeyedObjectPool
	{
	public:
		typedef std::function<Value *(Key)> funcCreator_t;
		typedef std::function<void(Value *)> funcDeleter_t;

	protected:
		struct object
		{
			std::unique_ptr<Value, funcDeleter_t> val;
			typename std::multimap<Key, typename std::list<object>::iterator>::iterator iterMap;
		};

	public:
		class pool_ptr : protected std::unique_ptr<Value, funcDeleter_t>
		{
			friend class KeyedObjectPool<Key, Value>;
			using Base = std::unique_ptr<Value, funcDeleter_t>;
		public:
			pool_ptr(pool_ptr &&other) : Base(std::move(other)), key(std::move(other.key)), pool(other.pool) {}
			pool_ptr(const pool_ptr &other) = delete;

			pool_ptr &operator=(pool_ptr &&rhs)
			{
				if (&rhs == this)
					return *this;
				key = rhs.key;
				pool = rhs.pool;
				Base::operator=(std::move(rhs));
				return *this;
			}

			Value & operator*() const
			{
				return Base::operator*();
			}
			Value * operator->() const
			{
				return Base::operator->();
			}
			Value * get() const
			{
				return Base::get();
			}
			void reset()
			{
				Base::reset();
			}
			Value * release()
			{
				return Base::release();
			}
			void returnObject()
			{
				pool->returnObject(key, Base::release());
			}

		protected:
			pool_ptr(Key key, Value *ptr, KeyedObjectPool *pool) : 
				Base(ptr, pool->funcDeleter), key(std::move(key)), pool(pool)
			{
			}

		protected:
			Key key;
			KeyedObjectPool *pool;
		};

	public:
		KeyedObjectPool(funcCreator_t funcCreator, funcDeleter_t funcDeleter, size_t sizePool) : funcCreator(funcCreator), funcDeleter(funcDeleter), sizePool(sizePool) { }
		KeyedObjectPool(const KeyedObjectPool &) = delete;

		pool_ptr borrowObject(const Key &key)
		{
			std::lock_guard<std::mutex> lock(mtx);

			auto iter = mapKV.find(key);
			if (iter == mapKV.end())
			{
				pool_ptr ret(key, funcCreator(key), this);
				if(ret.get())
					lentOutObject.insert(ret.get());
				return ret;
			}
			pool_ptr ret(key, iter->second->val.release(), this);
			listObjects.erase(iter->second);
			mapKV.erase(iter);
			lentOutObject.insert(ret.get());
			return ret;
		}

		void clear()
		{
			std::lock_guard<std::mutex> lock(mtx);
			listObjects.clear();
			mapKV.clear();
			lentOutObject.clear();
		}

	protected:
		void returnObject(const Key &key, Value *ptr)
		{
			std::lock_guard<std::mutex> lock(mtx);

			if (!lentOutObject.count(ptr))
			{
				funcDeleter(ptr);
				return;
			}
			lentOutObject.erase(ptr);

			listObjects.push_front(object{ std::unique_ptr<Value, funcDeleter_t>(ptr, funcDeleter) });
			auto iter = mapKV.insert(std::make_pair(key, listObjects.begin()));
			listObjects.front().iterMap = iter;

			while (listObjects.size() > sizePool)
			{
				mapKV.erase(listObjects.back().iterMap);
				listObjects.pop_back();
			}
		}

	protected:
		std::mutex mtx;
		size_t sizePool;
		funcCreator_t funcCreator;
		funcDeleter_t funcDeleter;
		std::list<object> listObjects;
		typename std::multimap<Key, typename std::list<object>::iterator> mapKV;
		std::set<Value *> lentOutObject;
	};
}

#endif
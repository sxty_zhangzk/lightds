#ifndef LIGHTDS_NETCLIENT_HPP
#define LIGHTDS_NETCLIENT_HPP

#include <boost/asio.hpp>
#include <condition_variable>
#include "pool.hpp"
#include "netmsg.hpp"


namespace LightDS
{
	class NetClient
	{
	protected:
		static const size_t bufferSize = 2048;

	protected:
		struct Address
		{
			std::string addr;
			std::uint16_t port;

			bool operator<(const Address &rhs) const { return std::tie(addr, port) < std::tie(rhs.addr, rhs.port); }
		};
		struct Connection
		{
			KeyedObjectPool<Address, boost::asio::ip::tcp::socket>::pool_ptr	socket;
			std::function<void(std::string /*buf*/, std::string /*error*/)>		callback;
			std::vector<char>	recvBuffer;
			NetMessage			recvMsg;
			bool				done;


			Connection(decltype(socket) socket, decltype(callback) callback) :
				socket(std::move(socket)), callback(callback), recvBuffer(bufferSize), done(false)
			{
				recvMsg.setMsgHandler([this](std::string data) 
				{
					this->callback(std::move(data), "");
					done = true; 
				});
			}
		};

	public:
		NetClient(size_t sizePool = 32, const std::string &localAddr = "") : 
			srvAlive(true),
			work(new boost::asio::io_service::work(service)), 
			pool(
				std::bind(&NetClient::createSocket, this, std::placeholders::_1), 
				std::bind(&NetClient::deleteSocket, this, std::placeholders::_1),
				sizePool),
			localAddr(localAddr)
		{
		}
		~NetClient()
		{
			std::unique_lock<std::mutex> lock(mtxAliveConn);
			srvAlive = false;

			lock.unlock();
			pool.clear();
			lock.lock();
			
			for (auto *socket : setAliveConn)
			{
				boost::system::error_code ec;
				socket->close(ec);
			}
			
			if (!setAliveConn.empty())
				cvAliveConn.wait(lock);

			work.reset();

			for (auto &thread : worker)
				thread.join();
		}

		std::string sendRequest(const std::string &msg, const std::string &ip, std::uint16_t port)
		{
			Address addr{ ip, port };
			auto socket = pool.borrowObject(addr);
			if (!socket.get())
			{
				throw std::runtime_error("Invalid socket");
			}
			while (!checkAlive(socket.get()))
				socket = pool.borrowObject(addr);

			//socket->send(boost::asio::buffer(NetMessage::sendMsg(msg)));
			boost::asio::write(*socket, boost::asio::buffer(NetMessage::sendMsg(msg)));

			bool done = false;
			std::string response;
			std::vector<char> buffer(bufferSize);
			NetMessage netmsg;
			netmsg.setMsgHandler([&done, &response](const std::string &data) { done = true; response = data; });
			while (!done)
			{
				size_t nLen = socket->receive(boost::asio::buffer(buffer));
				netmsg.onRecv(buffer, nLen);
			}

			socket.returnObject();
			return response;
		}

		void sendAsyncRequest(const std::string &msg, const std::string &ip, std::uint16_t port, 
			std::function<void(std::string /*buf*/, std::string /*error*/)> callback)
		{
			Address addr{ ip, port };
			std::shared_ptr<Connection> conn = std::make_shared<Connection>(pool.borrowObject(addr), callback);
			if (!conn->socket.get())
			{
				callback("", "Invalid socket");
				return;
			}
			while (!checkAlive(conn->socket.get()))
				conn = std::make_shared<Connection>(pool.borrowObject(addr), callback);;

			std::shared_ptr<std::string> sendBuffer = std::make_shared<std::string>(NetMessage::sendMsg(msg));
			//auto *socket = conn->socket.get();
			boost::asio::async_write(*conn->socket, boost::asio::buffer(*sendBuffer), [sendBuffer, conn](const boost::system::error_code &error, size_t)
			{
				onSend(conn, error);
			});
		}

		void Run(unsigned int nThread = std::thread::hardware_concurrency())
		{
			for (unsigned int i = 0; i < nThread; i++)
			{
				worker.emplace_back([this]()
				{
					service.run();
				});
			}
		}

	protected:
		bool checkAlive(boost::asio::ip::tcp::socket * socket)
		{
			char buffer;
			boost::asio::ip::tcp::socket::non_blocking_io set_non_block(true);
			socket->io_control(set_non_block);

			boost::system::error_code err;
			socket->receive(boost::asio::buffer(&buffer, 1), socket->message_peek, err);

			boost::asio::ip::tcp::socket::non_blocking_io set_block(false);
			socket->io_control(set_block);

			if (err == boost::asio::error::eof)
				return false;
			else if (err == boost::asio::error::would_block)
				return true;
			else if (err)
				throw boost::system::system_error(err);
			else
				return true;
		}

		static void onSend(std::shared_ptr<Connection> conn, const boost::system::error_code &error)
		{
			if (error)
			{
				conn->callback("", error.message());
				return;
			}
			//auto *socket = conn->socket.get();
			conn->socket->async_receive(boost::asio::buffer(conn->recvBuffer), [conn](const boost::system::error_code &error, size_t recvLen)
			{
				onRecv(conn, recvLen, error);
			});
		}

		static void onRecv(std::shared_ptr<Connection> conn, size_t recvLen, const boost::system::error_code &error)
		{
			if (error)
			{
				conn->callback("", error.message());
				return;
			}
			conn->recvMsg.onRecv(conn->recvBuffer, recvLen);
			if (conn->done)
				conn->socket.returnObject();
			else
			{
				//auto *socket = conn->socket.get();
				conn->socket->async_receive(boost::asio::buffer(conn->recvBuffer), [conn](const boost::system::error_code &error, size_t recvLen)
				{
					onRecv(conn, recvLen, error);
				});
			}
		}
		
		boost::asio::ip::tcp::socket * createSocket(Address remoteAddr)
		{
			using namespace boost::asio;

			std::lock_guard<std::mutex> lock(mtxAliveConn);

			if (!srvAlive)
				return nullptr;

			ip::tcp::endpoint endpoint(ip::address::from_string(remoteAddr.addr), remoteAddr.port);
			std::unique_ptr<ip::tcp::socket> socket;
			if (localAddr.empty())
				socket.reset(new ip::tcp::socket(service));
			else
				socket.reset(new ip::tcp::socket(service, ip::tcp::endpoint(ip::address::from_string(localAddr), 0)));

			socket->connect(endpoint);

			setAliveConn.insert(socket.get());
			return socket.release();
		}
		void deleteSocket(boost::asio::ip::tcp::socket * socket)
		{
			if (!socket)
				return;

			std::lock_guard<std::mutex> lock(mtxAliveConn);
			setAliveConn.erase(socket);
			delete socket;
			if (setAliveConn.empty())
				cvAliveConn.notify_all();
		}

	protected:
		std::mutex mtxAliveConn;
		std::condition_variable cvAliveConn;
		std::set<boost::asio::ip::tcp::socket *> setAliveConn;
		bool srvAlive;

		boost::asio::io_service service;
		std::unique_ptr<boost::asio::io_service::work> work;

		std::list<std::thread> worker;

		KeyedObjectPool<Address, boost::asio::ip::tcp::socket> pool;

		std::string localAddr;
	};
}

#endif

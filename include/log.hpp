#ifndef LIGHTDS_LOG_HPP
#define LIGHTDS_LOG_HPP

#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <mutex>
#include <utility>
#include <map>

namespace LightDS
{
	enum LogLevel : std::uint32_t
	{
		Debug,
		Info,
		Warning,
		Error,
		Critical,
		Fatal
	};

	struct LogWriter
	{
		virtual void write(const std::string &line, LogLevel level) = 0;
	};

	class StreamWriter : public LogWriter
	{
	public:
		StreamWriter(std::ostream &out, bool colored = true) :
			out(out), colored(colored)
		{
		}

		virtual void write(const std::string &line, LogLevel level) override
		{
			if (!colored)
				out << line;
			else
			{
				//TODO
				out << line;
			}
		}

	protected:
		std::ostream &out;
		bool colored;
	};

	class Log
	{
	public:
		Log(std::vector<LogWriter *> writers)
		{
			for (LogWriter *writer : writers)
			{
				this->writers.emplace_back();
				this->writers.back().first = writer;
			}
		}

	public:
		template<typename ...T>
		void log(LogLevel level, T &&...args)
		{
			static const std::map<std::uint32_t, std::string> mapLogLevel = {
				{Debug, "  DEBUG  "}, {Info, "   INFO  "}, {Warning, " WARNING "},
				{Error, "  ERROR  "}, {Critical, "CRITICAL "}, {Fatal, "  FATAL  "}
			};
			std::stringstream ss;
			
			time_t rawtime;
			time(&rawtime);
			tm * timeinfo = localtime(&rawtime);
			ss << asctime(timeinfo) << " " << mapLogLevel.find(level)->second << " ";

			format(ss, std::forward<T>(args)...);
			for (auto &pair : writers)
			{
				std::lock_guard<std::mutex> lock(pair.second);
				pair.first->write(ss.str(), level);
			}
		}
		void vlogf(LogLevel level, const char *fmt, va_list arglist)
		{
			char buffer[2048];
			vsnprintf(buffer, 2048, fmt, arglist);
			log(level, buffer);
		}
		void logf(LogLevel level, const char *fmt, ...)
		{
			va_list args;
			va_start(args, fmt);
			vlogf(level, fmt, args);
			va_end(args);
		}

	protected:
		template<typename T, typename ...Tother>
		void format(std::ostream &out, T &&arg, Tother &&...other)
		{
			out << arg;
			format(out, std::forward<Tother>(other)...);
		}
		void format(std::ostream &out) {}

	protected:
		std::list<std::pair<LogWriter *, std::mutex>> writers;
	};
}

#endif